## Cache
###sqli.py
This script exploits the SQL database.  There exists an expoit in the `add_edit_event_user.php` file that will leak 31 
bytes of the database at a time.  By exploiting this, then identifying the underlying database software, this script
can enumerate every table, then capture the schema of every table, then capture every row of every column of every
table.  Because of 28 character limit, a SQL cell with more than that much contents takes multiple calls.

Because there is no throttling on the target server, this enumeration can happen incredibly fast.

There is no threading attempting, and every message is done synchronously, requiring a message send->receive->write 
cycle before the next send.  For a production exploitation, a threaded system might be more appropriate.
